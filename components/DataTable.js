import React from 'react'
import { View, Text, TouchableOpacity, Alert, ToastAndroid, Dimensions } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler';
import { webService } from '../webService';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const DataTable = (props) => {
    const { cakeRecords, changeId } = props;
    const checkDelete = (id) => {
        console.log("current ID" + id);
        Alert.alert(
            "",
            "Are you sure",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "OK", onPress: () => deleteResult(id) }
            ],
            { cancelable: false }
        );

    }
    const deleteResult = (id) => {
        console.log(id + "fewfewf");
        let apiEndpoint = 'forms/deleteEntry';
        let header = webService.getHeaders({})
        //console.log(postArray)
        webService.post(apiEndpoint, { id }, header).then((response) => {
            // console.log(response);
            if (response.status === 200) {
                response = response.data;
                console.log(response);
                if (response.success) {
                    changeId(id);
                    // Alert.alert('', response.message);
                    ToastAndroid.show(response.message, ToastAndroid.TOP);
                } else {
                    Alert.alert('ERROR', response.message)
                }
            } else {
                Alert.alert('ERROR', 'Net is not working')
            }
        })
    }
    return (
        <View>
            <ScrollView style={{ maxHeight: windowHeight - 370, paddingBottom: 10 }}>
                <View style={{ marginTop: 10 }}>
                    <View style={{ backgroundColor: '#fff' }}>
                        <View style={{ flexDirection: 'row', borderBottomColor: '#e7e7e7', borderBottomWidth: 1, padding: 15, backgroundColor: '#d7d7d7' }}>
                            <View style={{ flex: 1 }}><Text>Cake</Text></View>
                            <View style={{ flex: 1 }}><Text style={{ textAlign: "center" }}>Name</Text></View>
                            <View style={{ flex: 1 }}><Text style={{ textAlign: "center" }}>Weight</Text></View>
                            <View style={{ flex: 1 }}><Text style={{ textAlign: "center" }}>Price</Text></View>
                            <View style={{ width: 30 }}><Text>Dlt</Text></View>
                        </View>
                    </View>
                    <View style={{ backgroundColor: '#fff', flexGrow: 1 }}>
                        {
                            cakeRecords.map((val, key) => {
                                return (
                                    <View key={key} style={{ flexDirection: 'row', borderBottomColor: '#e7e7e7', borderBottomWidth: 1, paddingHorizontal: 15, paddingVertical: 10 }}>
                                        <View style={{ flex: 1 }}><Text style={{ paddingVertical: 5, fontSize: 12 }}>{val.cakeName}</Text></View>
                                        <View style={{ flex: 1 }}><Text style={{ paddingVertical: 5, fontSize: 12, textAlign: "center" }}>{val.customerName}</Text></View>
                                        <View style={{ flex: 1 }}><Text style={{ paddingVertical: 5, fontSize: 12, textAlign: "center" }}>{val.cakeWeight}</Text></View>
                                        <View style={{ flex: 1 }}><Text style={{ paddingVertical: 5, fontSize: 12, textAlign: "center" }}>{val.price}</Text></View>
                                        <TouchableOpacity onPress={() => checkDelete(val.id)} style={{ width: 30, justifyContent: 'center' }}>
                                            <FontAwesomeIcon style={{ color: 'red' }} icon={faTrash} />
                                        </TouchableOpacity>

                                    </View>
                                )
                            })
                        }
                        {/* <View style={{ flex: 1 }}><Text style={{ paddingVertical: 5, fontSize: 10 }}>black forest</Text></View>
                    <View style={{ flex: 1 }}><Text style={{ paddingVertical: 5, fontSize: 10 }}>Khursheed momin</Text></View>
                    <View style={{ flex: 1 }}><Text style={{ paddingVertical: 5, fontSize: 10 }}>5</Text></View>
                    <View style={{ flex: 1 }}><Text style={{ paddingVertical: 5, fontSize: 10 }}>3500</Text></View>
                    <View style={{ flex: 1 }}><Text style={{ paddingVertical: 5, fontSize: 10 }}>12-12-2012</Text></View> */}

                    </View>
                </View >
            </ScrollView>
        </View>
    )
}

export default DataTable
