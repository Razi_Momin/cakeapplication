import React from 'react'
import { View, Text } from 'react-native'

export default function DateInput() {
    return (
        <View>
            <Item fixedLabel last>
                <Label style={{ color: '#fff' }}>Date</Label>
                <Input style={{ color: '#fff' }}
                    onFocus={showDatepicker}
                    value={clientDate} />
            </Item>
            <View>
                {show && (
                    <DateTimePicker
                        testID="dateTimePicker"
                        value={dateValidate}
                        mode={mode}
                        is24Hour={true}
                        display="calendar"
                        onChange={onChange}
                    />
                )}
            </View>
            {
                (!isSubmit) ? <Button onPress={submitForm}
                    title="Submit"
                    color="purple"
                    accessibilityLabel="Learn more about this purple button"
                    height='40'
                /> : <ActivityIndicator size="large" color="red" />
            }
        </View>
    )
}
