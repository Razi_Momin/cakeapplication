import React, { useState, useEffect } from 'react'
import { Text, View, Dimensions, Button, ToastAndroid, ScrollView, Alert, ActivityIndicator, ImageBackground, TouchableOpacity } from 'react-native'
import { Item, Input, Label, Icon } from 'native-base';
import { webService } from '../webService';
import DateTimePicker from '@react-native-community/datetimepicker';
import NetInfo from "@react-native-community/netinfo";
import InputField from './InputField';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faCalendar } from '@fortawesome/free-solid-svg-icons'
const window = Dimensions.get("window");
const screen = Dimensions.get("screen");
export default function CakeEntry({ navigation }) {
    const [dimensions, setDimensions] = useState({ window, screen });

    const onChangeNew = ({ window, screen }) => {
        setDimensions({ window, screen });
    };
    useEffect(() => {
        Dimensions.addEventListener("change", onChangeNew);
        return () => {
            Dimensions.removeEventListener("change", onChangeNew);
        };
    });
    // const windowWidth = Dimensions.get('window').width;
    // const windowHeight = Dimensions.get('window').height;
    const [clientDate, setClientDate] = useState('');
    const [dateValidate, setDateValidate] = useState(Date.parse(new Date()));
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [cakeName, setCakeName] = useState('');
    const [cakeWeight, setCakeWeight] = useState(1);
    const [customerName, setCustomerName] = useState('');
    const [price, setPrice] = React.useState('');
    const [isSubmit, setIsSubmit] = useState(false);
    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };
    const showDatepicker = () => {
        showMode('date');
    };
    const resetForm = () => {
        setCakeName('');
        setClientDate('');
        setCakeWeight('');
        setCustomerName('');
        setPrice(0);
    }
    const submitForm = () => {
        NetInfo.addEventListener(state => {
            if (!state.isConnected) {
                Alert.alert("You are offline!");
                return false
            } else {
                let errorMessage;
                let validation = false;
                if (cakeName === '') {
                    errorMessage = 'Please enter cake Name'
                } else if (cakeWeight === '') {
                    errorMessage = 'Please enter cake weight'
                } else if (customerName === '') {
                    errorMessage = 'Please enter customer name'
                } else if (price === '') {
                    errorMessage = 'Please enter price'
                } else if (clientDate.length < 1) {
                    errorMessage = 'Please enter date'
                } else {
                    setIsSubmit(true);
                    validation = true;
                    callApi();
                }
                !validation ? ToastAndroid.show(errorMessage, ToastAndroid.TOP) : null;
            }
        });

        // !validation ? Alert.alert('ALERT', errorMessage) : null;
    }
    const callApi = () => {
        let postArray = { cakeName, cakeWeight, customerName, price, clientDate };
        let apiEndpoint = 'forms/postRecords';
        let header = webService.getHeaders({})
        //console.log(postArray)
        webService.post(apiEndpoint, postArray, header).then((response) => {
            setIsSubmit(false);
            console.log(response);
            // console.log('respo aaya');
            if (response.status === 200) {
                response = response.data;
                if (response.success) {
                    resetForm();
                    Alert.alert('Success', response.message);
                    setTimeout(() => {
                        // navigation.navigate('Details');
                    }, 2000);
                    // Alert.alert('Success', response.message)
                } else {
                    Alert.alert('ERROR', response.message)
                }
            } else {
                Alert.alert('ERROR', 'Net is not working')
            }
        })
    }
    const onChange = (event, selectedDate) => {
        // console.log(selectedDate);
        const currentDate = selectedDate || dateValidate;
        setShow(Platform.OS === 'ios');
        var dateObj = currentDate;
        var month = ("0" + (dateObj.getMonth() + 1)).slice(-2)
        var day = ("0" + (dateObj.getDate())).slice(-2)
        var year = dateObj.getFullYear();
        var hours = new Date().getHours();
        var minutes = new Date().getMinutes();
        var seconds = new Date().getSeconds();
        hours = ("0" + hours).slice(-2);
        // newdate = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
        // const newdate = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`
        const newdate = `${year}-${month}-${day}`
        setClientDate(newdate);
        setDateValidate(Date.parse(new Date()));
        // console.log(newdate + "fefwefwew");

    }
    return (

        <ImageBackground style={{
            flex: 1,
            resizeMode: "cover",
            width: dimensions.window.width
        }} source={require('../assets/img/abc.jpg.webp')}>
            <ScrollView>
                <View>
                    <View style={{ padding: 15, flex: 1 }}>
                        <View style={{ padding: 15, backgroundColor: '#33333373', borderRadius: 5 }}>
                            <InputField changeValue={(e) => setCakeName(e)}
                                type="default" fieldName="Cake" fieldValue={cakeName} />
                            <InputField type="numeric" changeValue={(e) => setCakeWeight(e)}
                                fieldName="Kg" fieldValue={String(cakeWeight)} />
                            <InputField type="default" changeValue={(e) => setCustomerName(e)}
                                fieldName="Customer" fieldValue={customerName} />
                            <InputField type="numeric" changeValue={(e) => setPrice(e)}
                                fieldName="Price" fieldValue={String(price)} />
                            <View style={{ marginBottom: 10 }}></View>
                            <TouchableOpacity style={{ paddingVertical: 10 }} onPress={() => showDatepicker()} style={{ flexDirection: "row", alignItems: 'center', borderBottomWidth: 1, borderColor: '#e7e7e7', paddingBottom: 10, marginBottom: 10 }}>

                                <View style={{ width: 30, marginRight: 5 }}>
                                    {/* <Icon style={{color:'#fff'}} name='search' /> */}
                                    <FontAwesomeIcon style={{ color: '#fff' }} icon={faCalendar} />
                                </View>

                                <Text style={{ fontSize: 18, textAlign: "center", color: '#fff' }}>
                                    {(clientDate === '') ? 'Select Date' : clientDate}
                                </Text>
                                <View>
                                    {show && (
                                        <DateTimePicker
                                            testID="dateTimePicker"
                                            value={dateValidate}
                                            mode={mode}
                                            is24Hour={true}
                                            display="calendar"
                                            onChange={onChange}
                                        />
                                    )}
                                </View>
                            </TouchableOpacity>
                            {/* <Item fixedLabel last>
                                <Label style={{ color: '#fff' }}>Date</Label>
                                <Input style={{ color: '#fff' }}
                                    onFocus={showDatepicker}
                                    value={clientDate} />
                            </Item>
                            <View>
                                {show && (
                                    <DateTimePicker
                                        testID="dateTimePicker"
                                        value={dateValidate}
                                        mode={mode}
                                        is24Hour={true}
                                        display="calendar"
                                        onChange={onChange}
                                    />
                                )}
                            </View> */}
                            {
                                (!isSubmit) ? <Button onPress={() => submitForm()}
                                    title="Submit"
                                    color="purple"
                                    accessibilityLabel="Learn more about this purple button"
                                    height='40'
                                /> : <ActivityIndicator size="large" color="red" />
                            }
                        </View>
                    </View>
                </View>
            </ScrollView>
        </ImageBackground>

    )
}
