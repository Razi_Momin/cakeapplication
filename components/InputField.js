import React from 'react'
import { View, Text } from 'react-native'
import { Item, Input, Label } from 'native-base';
const InputField = (props) => {
    const { fieldName, fieldValue } = props;
    return (
        <Item fixedLabel>
            <Label style={{ color: '#fff' }}>{fieldName}</Label>
            <Input keyboardType={props.type} style={{ color: '#fff' }}
                onChangeText={tel => props.changeValue(tel)}
                value={String(fieldValue)} />
        </Item>
    )
}

export default InputField
