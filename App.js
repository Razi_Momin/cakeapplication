import React from 'react'
import CakeEntry from './components/CakeEntry'
import Deails from './components/Details'
import DataTable from './components/DataTable'
import SplashScreen from './components/SplashScreen'
import { Button, View, Text, ImageBackground } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();
export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="SplashScreen">
        <Stack.Screen options={{
          title: 'Cake Entry', headerStyle: {
            backgroundColor: '#33333373', shadowColor: 'transparent', borderWidth: 0,elevation: 0
          }
        }} name="CakeEntry" component={CakeEntry} />
        <Stack.Screen options={{ title: 'Dashboard',headerLeft: null }} name="Details" component={Deails} />
        <Stack.Screen options={{ title: 'Dashboard' }} name="DataTable" component={DataTable} />
        <Stack.Screen options={{ headerShown: false }} name="SplashScreen" component={SplashScreen} />
      </Stack.Navigator>
    </NavigationContainer>
    // <CakeEntry />
  )

}

