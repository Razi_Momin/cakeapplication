import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Alert, Dimensions, TouchableOpacity, ImageBackground, Button, ToastAndroid, ActivityIndicator, BackHandler } from 'react-native'
import DateTimePicker from '@react-native-community/datetimepicker';
import { Icon, Content, Item, Input } from 'native-base';
import { webService } from '../webService';
import DataTable from './DataTable';
import { ScrollView } from 'react-native-gesture-handler';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';
// const img  = require('../assets/img/bgImg.jpg');
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const CakeDetails = ({ navigation }) => {
    const isFocused = useIsFocused();
    const [date, setDate] = useState(new Date(1598051730000));
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [updateRecord, setUpdateRecord] = useState(0);
    const [cakeRecords, setCakeRecords] = useState([]);
    const [clientDate, setClientDate] = useState('');
    const [dateValidate, setDateValidate] = useState(Date.parse(new Date()));
    const [isSubmit, setIsSubmit] = useState(false);
    const filterValue = (id) => {
        setUpdateRecord(id);
        setCakeRecords(cakeRecords.filter(cakeId => cakeId.id !== id));
    }

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', () => true)
        return () =>
            BackHandler.removeEventListener('hardwareBackPress', () => true)
    }, [])
    const onChange = (event, selectedDate) => {
        if (!selectedDate !== undefined) {
            const currentDate = selectedDate || dateValidate;
            setShow(Platform.OS === 'ios');
            var dateObj = currentDate;
            //var month = dateObj.getMonth() + 1; //months from 1-12
            // var day = dateObj.getDate();
            var year = dateObj.getFullYear();
            // var hours = new Date().getHours();
            // var minutes = new Date().getMinutes();
            // var s
            var month = ("0" + (dateObj.getMonth() + 1)).slice(-2)
            var day = ("0" + (dateObj.getDate())).slice(-2)
            const newdate = `${year}-${month}-${day}`
            setClientDate(newdate);
            setDateValidate(Date.parse(new Date()));
        }

    };
    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const showTimepicker = () => {
        showMode('time');
    };
    const [productDetails, setProductDetails] = useState([]);
    const [orderCount, setorderCount] = useState(0);
    const [totalCount, setTotalCount] = useState(0);
    const [totalRevenue, setTotalRevenue] = useState(0);
    // console.log(productDetails);
    console.log(productDetails);
    useFocusEffect(
        React.useCallback(() => {
            setCakeRecords([]);
            setClientDate('');
            dashBoradData();
        }, [])
    );
    useEffect(() => {
        dashBoradData();
    }, [updateRecord])
    const dashBoradData = () => {
        let apiEndpoint = 'forms/getRecords';
        let header = webService.getHeaders({})
        //console.log(postArray)
        webService.get(apiEndpoint, header).then((response) => {
            // console.log(response);
            if (response.status === 200) {
                response = response.data;
                console.log(response.data);
                if (response.success) {
                    setProductDetails(response.data);
                    setTotalCount(response.data.count);
                    setTotalRevenue(response.data.totalRvenue);
                } else {
                    Alert.alert('ERROR', response.message)
                }
            } else {
                // Alert.alert('ERROR', 'Net is not working')
            }
        });
    }
    const checkValidation = () => {
        if (clientDate.length > 0) {
            getRecords();
        } else {
            ToastAndroid.show('Please enter valid date', ToastAndroid.TOP)
        }
    }
    const getRecords = () => {
        setIsSubmit(true);
        let postArray = { clientDate };
        let apiEndpoint = 'forms/getDataByDate';
        let header = webService.getHeaders({})
        //console.log(postArray)
        webService.post(apiEndpoint, postArray, header).then((response) => {
            console.log(response);
            // console.log('respo aaya');
            if (response.status === 200) {
                response = response.data;
                setIsSubmit(false);
                // console.log(response);
                if (response.success) {
                    //resetForm();
                    setCakeRecords(response.data);
                    // Alert.alert('Success', response.message);
                    setTimeout(() => {
                        // navigation.navigate('Details');
                    }, 2000);
                    // Alert.alert('Success', response.message)
                } else {
                    setCakeRecords([]);
                    Alert.alert('ERROR', response.message);
                }
            } else {
                Alert.alert('ERROR', 'Net is not working')
            }
        })
    }
    return (

        <ImageBackground source={require('../assets/img/abc.jpg.webp')} style={{
            flex: 1,
            resizeMode: "cover"
        }}>
            <View style={{ padding: 15, backgroundColor: '#694DD5' }}>
                <View style={{ backgroundColor: '#fff', marginTop: 10, borderRadius: 5 }}>
                    <View>
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            paddingVertical: 15,
                        }}>
                            <View style={{ width: '50%', borderColor: '#e7e7e7', borderRightWidth: 1 }}>
                                <Text style={{ textAlign: "center", fontSize: 20, color: 'green' }}>{'\u20B9'} {totalRevenue === null ? 0 : totalRevenue}</Text>
                                <Text style={{ textAlign: 'center' }}>REVENUE</Text>
                            </View>
                            <View style={{ width: '50%' }}>
                                <View>
                                    <Text style={{ textAlign: "center", fontSize: 20, color: 'blue' }}> {totalCount}</Text>
                                    <Text style={{ textAlign: 'center' }}>ORDERS</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: windowWidth - 30, borderColor: '#e7e7e7', borderTopWidth: 1 }}></View>
                        <View style={{ paddingVertical: 3 }}>

                            <TouchableOpacity onPress={() => navigation.navigate('CakeEntry')}
                                style={styles.button}
                            >
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={{ fontSize: 14 }}>ADD ENTRY</Text>
                                    <View style={{ marginLeft: 10 }}><Icon style={{ color: 'blue' }} name='grid' /></View>
                                </View>
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>

            </View>

            <View style={{ marginTop: 20, padding: 15, backgroundColor: '#fff' }}>
                <TouchableOpacity onPress={showDatepicker} style={{ flexDirection: "row", alignItems: 'center', borderBottomWidth: 1, borderColor: '#e7e7e7', paddingBottom: 10, marginBottom: 10 }}>
                    <View style={{ width: 30, marginRight: 15 }}>
                        <Icon name='search' />
                    </View>

                    <Text style={{ fontSize: 18, textAlign: "center" }}>
                        {(clientDate === '') ? 'Select Date' : clientDate}
                    </Text>
                    <View>
                        {show && (
                            <DateTimePicker
                                testID="dateTimePicker"
                                value={dateValidate}
                                mode={mode}
                                is24Hour={true}
                                display="calendar"
                                onChange={onChange}
                            />
                        )}
                    </View>
                </TouchableOpacity>
                {
                    (!isSubmit) ? <Button onPress={() => checkValidation()}
                        title="Submit"
                        color="purple"
                        accessibilityLabel="Learn more about this purple button"
                        height='40'
                    /> : <ActivityIndicator size="large" color="red" />
                }

                <View>
                    {cakeRecords.length > 0 ? <DataTable changeId={(e) => filterValue(e)} cakeRecords={cakeRecords} /> : null}
                </View>
            </View>
        </ImageBackground >

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        paddingHorizontal: 10
    },
    button: {
        alignItems: "center",
        backgroundColor: "#fff",
        padding: 10
    },
    countContainer: {
        alignItems: "center",
        padding: 10
    }
});

export default CakeDetails


