import React from 'react'
import { View, Text, ImageBackground } from 'react-native'

export default function SplashScreen({ navigation }) {
    setTimeout(() => {
        navigation.navigate('Details');
    }, 2000);
    return (
        <ImageBackground style={{
            flex: 1,
            resizeMode: "cover",
            width: '100%',
            height: '100%',
        }} source={require('../assets/img/cake.jpg')}>
        </ImageBackground>
    )
}

